package pjwstk.data.mathfunction.trigonometry;

import pjwstk.data.mathfunction.Maths;

public class Sinus {
	public static double sinus(double degree) {

		double tmpDegree = 0;
		int accuracy = 10;
		double result = 0;

		switch (whitchQuarter(degree)) {
		case 1: {
			tmpDegree = Maths.parseDegreesToRadians(degree);
			break;
		}
		case 2: {
			tmpDegree = Math.PI - Maths.parseDegreesToRadians(degree);
			break;
		}
		case 3: {
			tmpDegree = Math.PI - Maths.parseDegreesToRadians(degree);
			break;
		}
		case 4: {
			tmpDegree = 2 * Math.PI - Maths.parseDegreesToRadians(degree);
			break;
		}

		default:
			break;
		}

		for (int i = 0; i < accuracy; i++) {

			result = result + Math.pow(-1, i) * Math.pow(tmpDegree, 2 * i + 1) / Maths.Strong(2 * i + 1);
			System.out.println("result = "+result);
			System.out.println("i="+i+":"+Math.abs(result - Math.sin(Maths.parseDegreesToRadians(degree))));

		}

		return result;
	}

	static int whitchQuarter(double degree) {

		double tmpDegree;
		if (degree > 360) {

			tmpDegree = (degree / 360) * Math.pow(10, 5) - ((int) (degree / 360)) * Math.pow(10, 5);
			tmpDegree = (tmpDegree / Math.pow(10, 5)) * 360;

		} else
			tmpDegree = degree;

		if (tmpDegree >= 0 && tmpDegree <= 90)
			return 1;
		if (tmpDegree > 90 && tmpDegree <= 180)
			return 2;
		if (tmpDegree > 180 && tmpDegree <= 270)
			return 3;
		if (tmpDegree > 270 && tmpDegree <= 360)
			return 4;
		return -1;
	}
}