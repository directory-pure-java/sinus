package pjwstk.data.mathfunction;

import pjwstk.data.Unit;

public class Maths {

	public static double sinus(double degree, Unit unit) {

		double tmpDegre = 0;
		int accuracy = 10;
		double result = 0;

		switch (whitchQuarter(degree, unit)) {
		case 1: {
			if (unit.equals(Unit.DEGREES))
				tmpDegre = Maths.parseDegreesToRadians(degree);
			else {
				tmpDegre = degree;

			}
			break;
		}
		case 2: {
			if (unit.equals(Unit.DEGREES))
				tmpDegre = Math.PI - Maths.parseDegreesToRadians(degree);
			else
				tmpDegre = Math.PI - degree;
			break;
		}
		case 3: {
			if (unit.equals(Unit.DEGREES))
				tmpDegre = Math.PI - Maths.parseDegreesToRadians(degree);
			else
				tmpDegre = Math.PI - degree;
			break;
		}
		case 4: {
			if (unit.equals(Unit.DEGREES))
				tmpDegre = 2 * Math.PI - Maths.parseDegreesToRadians(degree);
			else
				tmpDegre = 2 * Math.PI - degree;
			break;
		}

		default:
			break;
		}

		System.out.println("-----------------------------------");
		System.out.println(" Accuracy  |   Precision");
		System.out.println("-----------------------------------");
		for (int i = 0; i < accuracy; i++) {

			result = result + Math.pow(-1, i) * Math.pow(tmpDegre, 2 * i + 1) / Maths.Strong(2 * i + 1);

			if (unit.equals(Unit.DEGREES))
				System.out.println(
						"i=" + i  +":       | " + Math.abs(result - Math.sin(Maths.parseDegreesToRadians(degree))));
			else
				System.out.println("i=" + i+  ":       | " + Math.abs(result - Math.sin((degree))));

		}

		System.out.println("-----------------------------------");
		return result;
	}

	static int whitchQuarter(double degree, Unit unit) {

		double tmpDegree = 0;

		if (unit.equals(Unit.DEGREES)) {
			if (degree > 360) {

				tmpDegree = (degree / 360) * Math.pow(10, 10) - ((int) (degree / 360)) * Math.pow(10, 10);
				tmpDegree = (tmpDegree / Math.pow(10, 10)) * 360;

			} else {
				tmpDegree = degree;

			}
		}

		if (unit.equals(Unit.RADIANS)) {

			double tmpConvertedRadiansToDegree = parseRadiansToDegrees(degree);

			if (tmpConvertedRadiansToDegree > 360) {

				tmpDegree = (tmpConvertedRadiansToDegree / 360) * Math.pow(10, 10)
						- ((int) (tmpConvertedRadiansToDegree / 360)) * Math.pow(10, 10);
				tmpDegree = (tmpDegree / Math.pow(10, 10)) * 360;
				degree = parseDegreesToRadians(tmpDegree);

			} else
				tmpDegree = tmpConvertedRadiansToDegree; // stopnie
			degree = parseDegreesToRadians(tmpDegree);

		}

		if (tmpDegree >= 0 && tmpDegree <= 90)
			return 1;
		if (tmpDegree > 90 && tmpDegree <= 180)
			return 2;
		if (tmpDegree > 180 && tmpDegree <= 270)
			return 3;
		if (tmpDegree > 270 && tmpDegree <= 360)
			return 4;
		return -1;
	}

	public static double Strong(int counter) {
		int sum = 1;
		for (int i = 1; i <= counter; i++) {
			sum = sum * i;
		}
		return sum;
	}

	public static double parseDegreesToRadians(double angles) {

		return angles * Math.PI / 180;
	}

	public static double parseRadiansToDegrees(double angles) {

		return angles * 180 / Math.PI;
	}

}
