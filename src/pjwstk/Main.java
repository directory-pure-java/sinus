package pjwstk;

import java.util.Scanner;

import javax.lang.model.element.Element;

import pjwstk.data.Unit;
import pjwstk.data.mathfunction.Maths;

/**
 * @author WB
 *
 */
public class Main extends Exception {

	public static void main(String[] args) throws Exception {

		Unit unit = null;

		System.out.print("Set unit: ");
		Scanner scan = new Scanner(System.in);
		System.out.print("radians (y/n)?: ");
		String dataString = scan.nextLine();

		if (dataString.equals("y"))
			unit = Unit.RADIANS;
		else if (	dataString.equals("n"))
			unit = Unit.DEGREES;
		else
		{throw new Exception(" y or n is the only choice");}

		System.out.print("Enter value in " + unit + ": ");
		scan = new Scanner(System.in);
		dataString = scan.nextLine();
		// String data_value = scan.nextLine();
		scan.close();
		System.out.println("sin(" + dataString +"["+ unit + "]"+") = " + Maths.sinus(Double.parseDouble(dataString), unit));
	}

}
